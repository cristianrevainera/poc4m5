package com.example.skip.poc4m7.controller;

import com.example.skip.poc4m7.dto.OrderDTO;
import com.example.skip.poc4m7.exceptions.ErrorEnum;
import com.example.skip.poc4m7.exceptions.ErrorWrapper;
import com.example.skip.poc4m7.exceptions.ExternalServiceException;
import com.example.skip.poc4m7.services.OrderService;
import org.junit.Before;
import org.junit.Test;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import javax.mail.internet.MimeMessage;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;

public class OrderControllerTest {

    private Long order1Id;
    private String order1Email;
    private OrderDTO order1;

    private OrderService orderServiceMock;
    private JavaMailSender javaMailSenderMock;

    private OrderController orderController;


    @Before
    public void setup() {
        order1Id = 1L;
        order1Email = "test1@gmail.com";
        order1 = new OrderDTO();
        order1.setEmail(order1Email);

        orderServiceMock = mock(OrderService.class);
        javaMailSenderMock = mock(JavaMailSender.class);

        orderController = new OrderController(javaMailSenderMock, orderServiceMock);
    }

    @Test
    public void save() throws ExternalServiceException {

        doReturn(order1).when(orderServiceMock).getById(order1Id);

        //MimeMessage minemeMessageMock = mock(MimeMessage.class);
        JavaMailSenderImpl javaMailSender = new JavaMailSenderImpl();
        MimeMessage minemeMessage = javaMailSender.createMimeMessage();

        doReturn(minemeMessage).when(javaMailSenderMock).createMimeMessage();

        ResponseEntity responseEntity = orderController.send(order1Id, "inserted");

        assertEquals(HttpStatus.OK.value(), responseEntity.getStatusCode().value());
     }

     @Test
     public void throwExternalServiceException() throws ExternalServiceException {

         ExternalServiceException externalServiceExceptionMock = mock(ExternalServiceException.class);

         doThrow(externalServiceExceptionMock).when(orderServiceMock).getById(order1Id);

         ResponseEntity responseEntity = orderController.send(order1Id, "inserted");

         ErrorWrapper errorWrapper =  (ErrorWrapper) responseEntity.getBody();

         assertEquals(ErrorEnum.JMS_ERROR.getMsg(), errorWrapper.getMessage());
     }

//    @Test
//    public void throwMessagingException() throws ExternalServiceException {
//
//        MessagingException messagingExceptionMock = mock(MessagingException.class);
//        ......
//        ResponseEntity responseEntity = orderController.send(order1Id, "inserted");
//
//        ErrorWrapper errorWrapper =  (ErrorWrapper) responseEntity.getBody();
//
//        assertEquals(ErrorEnum.JMS_ERROR.getMsg(), errorWrapper.getMessage());
//    }


}
