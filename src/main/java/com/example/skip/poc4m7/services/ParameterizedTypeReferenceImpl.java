package com.example.skip.poc4m7.services;

import com.example.skip.poc4m7.dto.OrderDTO;
import org.springframework.core.ParameterizedTypeReference;

import java.util.List;

/**
 * Parameterized Type Reference implementation
 */
public class ParameterizedTypeReferenceImpl extends ParameterizedTypeReference<List<OrderDTO>> {


}
