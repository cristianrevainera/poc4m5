package com.example.skip.poc4m7.services;

import com.example.skip.poc4m7.dto.OrderDTO;
import com.example.skip.poc4m7.exceptions.ExternalServiceException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.List;

/**
 * Product Service layer implementation
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Value("${poc4m1.url}")
    private String poc4m1Url;

    private RestTemplate restTemplate;

    /**
     * Product service constructor
     * @param restTemplate {@link RestTemplate} rest template
     */
    @Autowired
    public OrderServiceImpl(final RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    /**
     * Retrieves a product by an identification number
     * @param id (@link Long) ProductDTO's identification number
     * @return ProductDTO {@link OrderDTO} return a product
     * @throws ExternalServiceException
     */
    @Override
    public OrderDTO getById(final Long id) throws ExternalServiceException {
        return restTemplate.getForObject(poc4m1Url + "/orders/{id}", OrderDTO.class, id);
    }

    /**
     * Retrieves all products list
     * @return ProductDTO {@link OrderDTO} return a product list
     */
    @Override
    public List<OrderDTO> getAll() {
        ResponseEntity<List<OrderDTO>> response = restTemplate.exchange(
                poc4m1Url + "/orders",
                HttpMethod.GET,
                null,
                new ParameterizedTypeReferenceImpl());
        return response.getBody();
    }

}
