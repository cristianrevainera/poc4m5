package com.example.skip.poc4m7.services;

import com.example.skip.poc4m7.dto.OrderDTO;
import com.example.skip.poc4m7.exceptions.ExternalServiceException;

import java.util.List;

/**
 * ProductDTO service layer definition
 */
public interface OrderService {

    /**
     * Retrieves a ProductDTO with the given id
     * @param id (@Link Long) ProductDTO's identification number
     * @return (@Link ProductDTO) the found ProductDTO
     * @throws ExternalServiceException An exception if the ProductDTO doesn't exist in the database
     */
    OrderDTO getById(Long id) throws ExternalServiceException;

    /**
     * Retrieves all the ProductDTOs
     * @return (@Link ProductDTO) a list of ProductDTOs
     */
    List<OrderDTO> getAll();
}
