package com.example.skip.poc4m7.controller;


import com.example.skip.poc4m7.dto.OrderDTO;
import com.example.skip.poc4m7.exceptions.ErrorEnum;
import com.example.skip.poc4m7.exceptions.ErrorWrapper;
import com.example.skip.poc4m7.exceptions.ExternalServiceException;
import com.example.skip.poc4m7.services.OrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;


/**
 * Product Controller provides endpoint to add product in bulk to an ActiveMQ topic
 */
@RestController
public class OrderController {

    private static final Logger LOGGER =  LoggerFactory.getLogger(OrderController.class);

    private OrderService orderService;

    private JavaMailSender javaMailSender;

    /**
     * Order controller constructor
     * @param javaMailSender {@link JavaMailSender} java mail sender
     * @param orderService {@link OrderService} order service
     */
    @Autowired
    public OrderController(final JavaMailSender javaMailSender, final OrderService orderService) {
        this.javaMailSender = javaMailSender;
        this.orderService = orderService;
    }

    /**
     * Post
     * @param orderId (@link List<Long>) order id List
     * @param status {@link String} status
     * @return (@link ResponseEntity) ResponseEntity with http status code 200
     */
    @PostMapping(value = "/order")
    public ResponseEntity send(@RequestParam(value = "orderId", required = true)  final Long orderId,
                               @RequestParam(value = "status", required = true)  final String status) {
        try {
            OrderDTO orderDTO = orderService.getById(orderId);

            MimeMessage message = javaMailSender.createMimeMessage();
            MimeMessageHelper helper = new MimeMessageHelper(message);
            helper.setTo(orderDTO.getEmail());
            helper.setText("Order status: " + status);
            helper.setSubject("poc4m7 order  " + orderDTO.getId() + " status.");
            javaMailSender.send(message);

            return new ResponseEntity<>(HttpStatus.OK);

        } catch (ExternalServiceException | MessagingException e) {
            LOGGER.error(ErrorEnum.JMS_ERROR.getMsg());
            return new ResponseEntity(new ErrorWrapper(ErrorEnum.JMS_ERROR.getMsg()), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
