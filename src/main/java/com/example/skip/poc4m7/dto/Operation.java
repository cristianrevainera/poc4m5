package com.example.skip.poc4m7.dto;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 *
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum Operation {
    INSERT("insert"),
    UPDATE("update"),
    DELETE("delete");

    private String operation;

}
