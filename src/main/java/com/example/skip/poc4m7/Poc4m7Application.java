package com.example.skip.poc4m7;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.jms.annotation.EnableJms;

/**
 * Prof of concept application
 */
@SpringBootApplication
@EnableJms
public class Poc4m7Application {
/**
 * Root method
 * @param args command line parameters
 */
public static void main(final String[] args) {
SpringApplication.run(Poc4m7Application.class, args);
}
}
