package com.example.skip.poc4m7.exceptions;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * Enum con los errores (una cuestion de prolijidad.
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PUBLIC)
public enum ErrorEnum {
    JSON_MARSHALLING_ERROR("Error haciendo marshalling del Json."),
    JMS_ERROR("Java message service error.");

    private String msg;

}
