package com.example.skip.poc4m7.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Exception creada especificamente para el manejo de nuestras exceptions.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ExternalServiceException extends Exception {
    /**
     *
     * @param s {@link String}
     */
    public ExternalServiceException(final String s) {
        super(s);
    }

    /**
     *
     * @param s {@link String}
     * @param throwable {@link Throwable}
     */
    public ExternalServiceException(final String s, final Throwable throwable) {
        super(s, throwable);
    }
}
